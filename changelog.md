# Changelog #

0.0.2: Added more applications
* /app/Stk
* /product/priv-app/OneTimeInitializer
* /system_ext/priv-app/LineageSetupWizard
* /system_ext/priv-app/TrebuchetQuickStep

0.0.1: Initial release